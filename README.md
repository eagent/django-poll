Django-Poll
===========

## Bitbucket Pipeline CD Dokku

### Create a dokku app

```bash

$ dokku apps:create django-poll
```

### Add Dokku Remote URL to Bitbucket repository variables

From repository Settings go to Repository variables and add the following variable.

Variable name: `DOKKU_REMOTE_URL`

value: `dokku@<dokku_host_domain_name>:django-poll`


### Generate Bitbucket Pipeline SSH Key

From repository settings go to SSH Keys then click Generate SSH Key button. 
It will create a pair of ssh keys.

Just bellow the SSH key pair there is a filed for adding dokku host. 
Don't forget to click the button Fetch. Now add the host by clicking Add Host button.
 
Now add the SSH public key to the dokku host.

Save the public key on your local system - `pipeline_id_rsa.pub`


```bash

$ cat /path_to/pipeline_id_rsa.pub | ssh root@dokku_host_domain_name "sudo sshcommand acl-add dokku [pipeline]"

```